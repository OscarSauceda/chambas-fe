const webpackMerge = require('webpack-merge');
const baseConfig = require('./webpack.config.base.js');

const devConfig = {}

module.exports = webpackMerge(baseConfig, devConfig);
