const webpackMerge = require('webpack-merge');
const baseConfig = require('./webpack.config.base.js');
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const webpack = require('webpack');

const prodConfig = {
  plugins: [
    new UglifyJSPlugin({
      parallel: true,
      sourceMap: true,
      uglifyOptions: {
        beautify: false,
        ecma: 6,
        compress: {
          drop_console: true
        },
        comments: false,
        mangle: true,
      }
    }),
    new webpack.SourceMapDevToolPlugin({
      filename: '[name].js.map'
    })
  ]
};

module.exports = webpackMerge(baseConfig, prodConfig);
