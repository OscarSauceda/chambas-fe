const webpack = require('webpack');
const glob = require('glob');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const jsViews = glob.sync('./dev/js/views/**/*.js');

const config = {
  entry: {
    master: './dev/js/master.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(`${__dirname}/../public/assets`, 'js'),
    publicPath: path.resolve(`${__dirname}/../public/assets`, 'js')
  },
  resolveLoader: {
    modules: ["node_modules"]
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: { sourceMap: true }
            },
            { loader: 'sass-loader',
              options: { sourceMap: true }
            }
          ]
        })
      },
      {
        enforce: "pre",
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'eslint-loader'
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ["react", "es2015", "stage-0", "stage-3"],
              comments: false
            }
          }
        ]
      }
    ]
  },
  devtool: 'source-map',
  plugins: [
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new ExtractTextPlugin('../css/master.css')
  ]
};

if (jsViews.length) {
  for (var i = 0; i < jsViews.length; i++) {
    config.entry[jsViews[i].replace('./dev/js/','./').replace('.js','')] = jsViews[i];
  }
}

module.exports = config;
