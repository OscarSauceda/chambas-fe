'use strict';

module.exports = [
  {
    method: 'GET',
    path: '/{any*}',
    handler: (request, h) => h.view('404').code(404)
  }
];
