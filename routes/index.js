'use strict';

// Requiring all routes
const views = require('./views.js');
const assets = require('./assets.js');
const home = require('./home.js');
const error = require('./404.js');

// Merge and expose all routes.
module.exports = [...views, ...assets, ...home, ...error];
