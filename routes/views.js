'use strict';

// dependencies
const routeHelpers = require('./helpers/index');

const routes = [
  {
    path: '/dashboard',
    template: 'dashboard'
  },
  {
    path: '/job-creation',
    template: 'job-creation'
  },
  {
    path: '/profile',
    template: 'profile'
  },
  {
    path: '/newsletter-subscribe',
    template: 'subscribe',
    context: {
      noNavigation: true
    }
  },
  {
    path: '/task-collection',
    template: 'task-collection'
  },
  {
    path: '/task/{type}',
    template: 'task-type',
    context: {
      noNavigation: true
    }
  },
  {
    path: '/provider',
    template: 'provider'
  },
  {
    path: '/task/pick-provider',
    template: 'pick-provider',
    context: {
      noNavigation: true
    }
  },
  {
    path: '/task-form',
    template: 'task-form',
    context: {
      noNavigation: true
    }
  },
  {
    path: '/task-form/thank-you',
    template: 'task-form-thank-you'
  }
];

module.exports = routeHelpers.objectCreator(routes);

// module.exports = [
//   {
//     method: 'GET',
//     path: '/{foldername}',
//     handler: (request, h) => h.view(`${encodeURIComponent(request.params.foldername)}`)
//   }
// ];