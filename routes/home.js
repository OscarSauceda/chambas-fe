'use strict';

module.exports = [
  {
    method: 'GET',
    path: '/',
    config: {
      handler: {
        view: {
        	template: 'index'
        }
      }
    }
  }
];