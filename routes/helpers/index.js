'use strict';

/**
 * object containing helper methods to create routes
 * @type {Object}
 */
const helpers = {
  /**
   * method that creates an array of routes
   * @param  {[Array]} routes) array of objects containing the URI and template per route.
   * @return {[Array]}         array of objects with the specific configuration per route.
   */
  objectCreator: (routes) => routes.map((el, index) => ({
    method: 'GET',
    path: el.path,
    handler (request, h) {

      return h.view(el.template, {params: request.params, ...el.context})
    }
  }))
};

module.exports = helpers;


/*

      const view = {
        template: el.template,
        options: { ...el.options },
        context: { ...el.context, param: request.params.type }
      }
*/