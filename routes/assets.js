'use strict';

module.exports = [
  {
    method: 'GET',
    path: '/assets/{file*}',
    handler: {
      directory: {
        path: './assets'
      }
    }
  }
];
