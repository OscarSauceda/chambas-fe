'use strict';

// Dependencies
const Hapi = require('hapi');
const Inert = require('inert');
const Path = require('path');
const Vision = require('vision');
const Handlebars = require('handlebars');
const config = require('config');

// Importing Routes
const routes = require('./routes/index');

// Internals
const internals = {};
const today = new Date();

internals.thisYear = today.getFullYear();

// Creating new hapi server instance
const server = new Hapi.Server({
  port: config.get('app.port'),
  host: 'localhost',
  debug: {
    request: ['error']
  },
  routes: {
    files: {
      relativeTo: Path.join(__dirname, 'public')
    }
  },
  router: {
    stripTrailingSlash: true
  }
});

// Server specific configuration
internals.provision = async () => {

  await server.register([Inert, Vision]);

  server.views({
    engines: { hbs: Handlebars },
    relativeTo: __dirname,
    path: './public/templates/views/',
    layoutPath: './public/templates/layouts/',
    partialsPath: './public/templates/partials/',
    layout: 'default',
    context: {
      year: internals.thisYear
    }
  });

  server.route(routes);

  try {
    await server.start();
  }
  catch (err) {
    console.log(err);
  }

  console.log(`  🖥   Server running at: ${server.info.uri}`);
}

// Kickstarting server!
internals.provision();
