import _ from 'utils';

// Local Scope ==================================
const internals = {
  $arrow: document.querySelector('.arrow-wrapper')
};

internals.scrollFunction = () => {
  const { $arrow } = internals;
  const $bodyScroll = (document.body.scrollTop) ? document.body.scrollTop : document.documentElement.scrollTop;

  if ( $bodyScroll > 20 ) {
    _.removeClass($arrow, 'zero');
    _.addClass($arrow, 'show-arrow');
  } else {
    _.removeClass($arrow, 'show-arrow');
    _.addClass($arrow, 'zero');
  }
};
export default () => {
  const { scrollFunction } = internals;

  window.onscroll = () => { scrollFunction(); };
};
