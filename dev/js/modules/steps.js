// This is a temporal solution, it has to be refactored later in project development.
const internals = {
  hrefLocation: (window.location.pathname === '/') ? 'vacation' : window.location.pathname.slice(1, -5),
  $stepWrapper: document.querySelector('.steps-wrapper')
};

internals.toggler = () => {

  // First check if ".steps-wrapper" exists in the DOM, if not present, do a return.
  if (internals.$stepWrapper === null) {
    return;
  }

  const $currentStep =  internals.$stepWrapper.querySelector(`.${internals.hrefLocation}-step`);
  const $stepsWrapperChildren = Array.from(document.querySelector('.steps-wrapper .row').children);

  for ( const children of $stepsWrapperChildren) {

    if (children === $currentStep) {
      _.addClass(children, 'current-step');

      return;
    }

    _.addClass(children, 'completed-step');
  }
};

export default() => {
  const { toggler } = internals;

  toggler();
};
