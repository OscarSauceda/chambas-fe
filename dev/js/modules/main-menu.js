// Deps =========================================
import _ from 'utils';

// Local Scope ==================================
const internals = {
  $menuTrigger: document.getElementById('mm-aside'),
  $subTriggers: document.getElementsByClassName('mm-trigger'),
  $subLabels: document.getElementsByClassName('mm-label'),
  $menuLinks: document.querySelectorAll('#js-main-menu a')
};

// Reset sub-menu triggers ======================
internals.onChangeBurgerTrigger = e => {

  const { $subTriggers } = internals;
  const $rstSubTrigger = document.querySelector('.mm-trigger');

  _.toggleClass(document.querySelector('body'), 'no-flow');

  // if opening, do nothing
  if (e.target.checked) {
    return;
  }

  // Uncheck radio btns
  for (let i = 0, len = $subTriggers.length; i < len; ++i) {
    $subTriggers[i].checked = false;
  }
  // Set resort sub-menu to be displayed by default
  $rstSubTrigger.checked = true;
};

// Sub-menu label click handler =================
internals.onClickSubLabels = e => {

  const $subTrigger = document.getElementById(e.currentTarget.htmlFor);

  // Check if $subTrigger doesnt exists or it exists and checked is `false`
  if ($subTrigger && !$subTrigger.checked) {
    // do nothing
    return;
  }

  e.preventDefault(); // Prevent label from overriding `checked = false`
  $subTrigger.checked = false;
};

// Sub-menu hover handler ======================
internals.onHoverSubLabels = e => {

  const $subTrigger = document.getElementById(e.currentTarget.htmlFor);
  const { $subTriggers } = internals;

  e.preventDefault();

  // Uncheck radio btns
  for (let i = 0, len = $subTriggers.length; i < len; ++i) {
    $subTriggers[i].checked = false;
  }

  $subTrigger.checked = true;
};

// Method to make each <a> redirect to Href location on touch devices
internals.onTouchHrefRedirect = e => {
  e.preventDefault();
  const hrefLocation = e.currentTarget.href;

  window.location.href = hrefLocation;
};

// Export menu helper kickstart =================
export default () => {

  const { $menuTrigger, $subLabels, onClickSubLabels, onChangeBurgerTrigger, onHoverSubLabels } = internals;

  // Enable JS for Main Menu
  _.addClass(document.getElementById('js-main-menu'), 'js');

  // Menu(burger) btn listener
  $menuTrigger.addEventListener('change', onChangeBurgerTrigger);

  for (let i = 0, len = $subLabels.length; i < len; ++i) {
    $subLabels[i].addEventListener('touchstart', onClickSubLabels);
    $subLabels[i].addEventListener('mouseover', onHoverSubLabels);
  }

  // TODO: Add event listeners to each <a> tag on touchstart event
};
