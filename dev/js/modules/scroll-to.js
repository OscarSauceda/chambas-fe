// scrollTargetY: the target scrollY property of the window
// speed: time in pixels per second
// easing: easing equation to use
// min time .1, max time .8 seconds

// Deps =========================================
import _ from 'utils';

// Local Scope ==================================
const internals = {
  $arrow: document.querySelector('.arrow-wrapper')
};

const requestAnimations = window.requestAnimFrame = ( () => {
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function callback(call) {
            window.setTimeout(call, 1000 / 60);
          };
})();

internals.scrollToY = (scrollTargetY, speed, easing) => {
  const scrollY = window.scrollY;
  const targetY = scrollTargetY || 0;
  const speedY = speed || 1000;
  const ease = easing || 'easeInOutSine';
  const time = Math.max(0.1, Math.min(Math.abs(scrollY - targetY) / speedY, 0.8));
  let currentTime = 0;
  const easingEquations = {
    easeOutSine: (pos) => {
      return Math.sin(pos * (Math.PI / 2));
    },
    easeInOutSine: (pos) => {
      return (-0.5 * (Math.cos(Math.PI * pos) - 1));
    },
    easeInOutQuint: (pos) => {
      const po = pos / 0.5;

      if ( po < 1) {
        return 0.5 * Math.pow(po, 5);
      }

      return 0.5 * (Math.pow((pos - 2), 5) + 2);
    }
  };

  function tick() {
    currentTime += 1 / 60;
    const p = currentTime / time;
    const t = easingEquations[ease](p);

    if (p < 1) {
      requestAnimations(tick);
      window.scrollTo(0, scrollY + ((targetY - scrollY) * t));
    } else {
      // console.log('scroll done');
      window.scrollTo(0, targetY);
    }
  }
  tick();
};

export default () => {
  const { $arrow, scrollToY } = internals;

  _.addListener($arrow, ['click', 'touchstart'], () => scrollToY(0, 1000, 'easeOutSine'));
};
