// Load Modules =================================
import mainMenu from './main-menu';
import fixedArrow from './fixed-arrow';
import scrollTo from './scroll-to';
import steps from './steps';

// Local scope ==============================
const internals = {};

// Generic event bindings ===================
internals.generalBindings = () => { };

// Doc Ready ================================
export default () => {

  const { generalBindings } = internals;

  generalBindings();
  mainMenu();
  fixedArrow();
  scrollTo();
  steps();
};
