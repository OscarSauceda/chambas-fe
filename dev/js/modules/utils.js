// Local scope ==================================
const internals = {};

// Capitalize first letters in every word =======
internals.capPhrase = (phrase) => phrase.replace(/_/g, ' ').replace(/\b[a-z](?=[a-z]{2})/g, letter => letter.toUpperCase());

// Rand!! wee! ==================================
internals.rand = () => (Math.random().toString(36) + '00000000000000000').slice(2, 8 + 2);

// Check if element contains className ==========
internals.hasClass = (el, className) => {

  if (el.classList) {
    return el.classList.contains(className);
  }

  return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
};

// Adds a className to an element ===============
internals.addClass = (el, className) => {

  if (el.classList) {
    el.classList.add(className);
  } else if (!internals.hasClass(el, className)) {
    el.className += ` ${className}`;
  }
};

// Removes a className to an element ============
internals.removeClass = (el, className) => {

  if (el.classList) {
    el.classList.remove(className);
  } else if (internals.hasClass(el, className)) {
    const reg = new RegExp('(\\s|^)' + className + '(\\s|$)');

    el.className = el.className.replace(reg, ' ');
  }
};

// Toggles a className to an element ============
internals.toggleClass = (el, className) => {

  if(el.classList) {
    el.classList.toggle(className);
  } else if (internals.hasClass(el, className)) {
    internals.removeClass(el, className);
  } else {
    internals.addClass(el, className);
  }
};

// Add multiple listeners =======================
internals.addListener = (el, events, handler) => {

  if (!(events instanceof Array)) {
    console.log('addMultipleListeners: please supply an array of eventstrings (like ["click","mouseover"])');

    return;
  }

  let event;

  for (event of events) {
    el.addEventListener(event, handler);
  }
};

internals.omit = (object, excludePropertiesArray) => {

  const returnObj = Object.assign({}, object);

  excludePropertiesArray.forEach((option) => {
    delete returnObj[option];
  });

  return returnObj;
};

// Export utils =================================
export default internals;
