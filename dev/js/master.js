// importing css
import '../sass/master.scss';

// Deps =========================================
import 'whatwg-fetch';
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import Validation from 'react-validation';
import Rules from 'validationRules';
import _ from 'utils';

// Adding Custom Rules ==========================
Object.assign(Validation.rules, Rules);

// Setting Global Vars ==========================
Object.assign(window, { React, ReactDOM, Validation, _ });

// _bind Helper =================================
React.Component.prototype._bind = function _bind(...methods) {

  methods.forEach((method) => {

    this[method] = this[method].bind(this);
  });
};

// Modules ======================================
import runGlobal from './modules/global';

// Kickstart app ================================
(() => {

  runGlobal();
})();
