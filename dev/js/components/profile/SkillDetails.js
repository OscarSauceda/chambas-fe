// Deps =========================================

// Internals ====================================
const Component = React.Component;

// Home-Panel
export default class MainPanel extends Component {
  render() {
    return (
      <div>
        <h6>Habilidades</h6>
        <ul className="skill-set">
          <li>Reparaciones de Electrodomesticos</li>
          <li>Instalaciones Electricas</li>
          <li>Reparacion Telefonos Moviles</li>
          <li />
        </ul>
      </div>
    );
  }
}
