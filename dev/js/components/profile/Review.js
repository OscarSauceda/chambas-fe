// Deps =========================================

// Internals ====================================
const Component = React.Component;

// Home-Panel

export default class MainPanel extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <div>
          <p className="comment-author">{this.props.commentor}</p>
          <p>
            <em>&quot;{this.props.comment}&quot;</em>
          </p>
          <div className="user-rating ">
            <svg
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              className="icon-rating"
              viewBox="0 0 32 32"
            >
              <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z" />
            </svg>
            <svg
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              className="icon-rating"
              viewBox="0 0 32 32"
            >
              <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z" />
            </svg>
            <svg
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              className="icon-rating"
              viewBox="0 0 32 32"
            >
              <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z" />
            </svg>
            <svg
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              className="icon-rating"
              viewBox="0 0 32 32"
            >
              <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z" />
            </svg>
            <svg
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              className="icon-rating"
              viewBox="0 0 32 32"
            >
              <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z" />
            </svg>
          </div>
        </div>
      </div>
    );
  }
}
