import React from 'react';
import PropTypes from 'prop-types';
import momentPropTypes from 'react-moment-proptypes';
import moment from 'moment';
import { SingleDatePicker, SingleDatePickerShape } from 'react-dates';
import { SingleDatePickerPhrases } from 'react-dates/lib/defaultPhrases';
import { HORIZONTAL_ORIENTATION, VERTICAL_ORIENTATION, ANCHOR_LEFT } from 'react-dates/constants';
import isInclusivelyAfterDay from 'react-dates/lib/utils/isInclusivelyAfterDay';
import utils from 'utils';

export default class DatePicker extends React.Component {

  static propTypes = {
    autoFocus: PropTypes.bool,
    initialDate: momentPropTypes.momentObj,

    ...utils.omit(SingleDatePickerShape,
      [
        'date',
        'onDateChange',
        'focused',
        'onFocusChange'
      ])
  };

  static defaultProps = {
    autoFocus: false,
    initialDate: null,

    // input related props
    id: 'date',
    placeholder: 'Date',
    disabled: false,
    required: false,
    screenReaderInputMessage: '',
    showClearDate: false,

    // calendar presentation and interaction related props
    orientation: window.innerWidth < 768 ? VERTICAL_ORIENTATION : HORIZONTAL_ORIENTATION,
    anchorDirection: ANCHOR_LEFT,
    horizontalMargin: 0,
    withPortal: false,
    withFullScreenPortal: window.innerWidth < 768,
    initialVisibleMonth: null,
    numberOfMonths: 2,
    keepOpenOnDateSelect: false,
    reopenPickerOnClearDate: false,

    // navigation related props
    navPrev: null,
    navNext: null,
    onPrevMonthClick() {},
    onNextMonthClick() {},

    // day presentation and interaction related props
    renderDay: null,
    enableOutsideDays: false,
    isDayBlocked: () => false,
    isOutsideRange: (day) => !isInclusivelyAfterDay(day, moment()),
    isDayHighlighted: () => {},

    // internationalization props
    displayFormat: () => moment.localeData().longDateFormat('L'),
    monthFormat: 'MMMM YYYY',
    phrases: SingleDatePickerPhrases,
    updateExternalDate: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      focused: props.autoFocus,
      date: props.initialDate,
    };

    this._bind('onDateChange', 'onFocusChange');
  }

  componentWillReceiveProps(props) {
    this.setState({ date: props.initialDate });
  }

  onDateChange(date) {
    this.setState({ date }, this.props.updateExternalDate(date));
  }

  onFocusChange({ focused }) {
    this.setState({ focused });
  }

  render() {
    const { focused, date } = this.state;

    const props = utils.omit(this.props, ['autoFocus', 'initialDate']);

    return (
      <div className="pr">
        <SingleDatePicker
          {...props}
          id="date_input"
          date={date}
          focused={focused}
          onDateChange={this.onDateChange}
          onFocusChange={this.onFocusChange}/>
        <div className="icon-calendar-2"/>
      </div>
    );
  }
}
