 // Deps =========================================
import React from 'react';
import momentPropTypes from 'react-moment-proptypes';
import moment from 'moment';

// Internal =====================================
const { Component, PropTypes } = React;

export default class Timer extends Component {
  static propTypes = {
    expiration: PropTypes.object,
    legend: PropTypes.node,
    auto: PropTypes.bool,
    intervalID: PropTypes.number,
    units: PropTypes.array
  };

  static defaultProps = {
    expiration: { hour: 23, minute: 59, second: 59 },
    legend: (<div><b>HURRY!</b><span>LAST DAY TO SAVE</span></div>),
    auto: true,
    intervalID: 0,
    units: ['seconds', 'minutes', 'hours', 'days'],
  };

  constructor(props) {
    super(props);

    this.state = {
      auto: props.auto,
      intervalID: props.intervalID,
      legend: props.legend,
      time: <div/>,
      expiration: (!moment.isDate(props.expiration) || !moment.isMoment(props.expiration))
      ? moment(props.expiration)
      : props.expiration
    };

    this._bind('getDiff', 'getMsg', 'start', 'pad', 'updateTimer');
  }

  componentDidMount() {

    this.start();
  }

  getDiff() {
    let dateDiff = this.state.expiration.diff(moment());

    // Getting the numbers for each unit
    const days = Math.floor(dateDiff / 1000 / 60 / 60 / 24);

    dateDiff -= (days * 1000 * 60 * 60 * 24);

    const hours = Math.floor(dateDiff / 1000 / 60 / 60);

    dateDiff -= (hours * 1000 * 60 * 60);

    const minutes = Math.floor(dateDiff / 1000 / 60);

    dateDiff -= (minutes * 1000 * 60);

    const seconds = Math.floor(dateDiff / 1000);

    dateDiff -= (seconds * 1000);

    const result = {
      days,
      hours,
      minutes,
      seconds
    };

    return result;
  }

  getMsg(dateDiff) {
    const result = {
      hurry: dateDiff.days < 1,
      msg: <div><b>HURRY!</b><span>ONLY {dateDiff.days} DAY{( dateDiff.days > 1) ? 'S' : ''} LEFT</span></div>
    };

    if (dateDiff.days < 1) {

      result.msg = <div><b>HURRY!</b><span>LAST DAY TO SAVE</span></div>;
    }

    return result;
  }

  start() {
    this.updateTimer();

    if (this.props.auto) {

      const intervalID = setInterval( () => {
        this.updateTimer();
      }, 1000);

      this.setState({ intervalID });
    }
  }

  pad(num) {

    return (`0${num}`).slice(-2);
  }

  updateTimer() {
    const dateDiff = this.getDiff();
    const msgObj = this.getMsg(dateDiff);
    const msgCls = `countdown-legend pr txt-center ${ msgObj.hurry ? 'hurry' : '' }`;
    const childrenArray = new Array();

    for (let i = this.props.units.length - 1; i >= 0; --i) {

      childrenArray.push(<div className={`xs-3 pr ${this.props.units[i]}`}><big>{this.pad(dateDiff[this.props.units[i]])}</big><small>{this.props.units[i].toUpperCase()}</small></div>);
    }

    this.setState({time: <div>
        <div className={msgCls}>
          {msgObj.msg}
        </div>
        <div className="cd-counter row">
          {childrenArray}
        </div>
      </div>
    });

  }

  render() {

    return (
      <div>
        {this.state.time}
      </div>
    );
  }
 }
