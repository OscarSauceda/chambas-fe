// Deps =========================================
import PropTypes from 'prop-types';

// Internal =====================================
const { Component } = React;

// Modal Component ===========================
export default class Modal extends Component {
  static propTypes = {
    isOpen: PropTypes.bool,
    containerClassName: PropTypes.string.isRequired,
    style: PropTypes.object
  };

  static defaultProps = {
    isOpen: false,
    overlayClassName: '',
    className: ''
  };

  constructor(props) {

    super(props);
    this._bind('_onClickClose', '_onKeyDownHandler');
  }

  state = {
    height: null
  };

  componentDidMount() {

    const mdlBox = document.querySelector('.mdl-box');
    let height = (mdlBox) ? `${mdlBox.clientHeight}px` : null;

    if (this.props.fullHeight) {
      height = '100%';
    }

    this.setState({ height });
  }

  componentDidUpdate() {

    const { isOpen, containerClassName } = this.props;
    const $openMdls = document.querySelectorAll('.mdl-wrap');

    if (isOpen && containerClassName) {

      const modalWrapper = document.querySelector(`.${containerClassName}`);

      modalWrapper.focus();
    }

    if ($openMdls.length === 0) {
      _.removeClass(document.querySelector('body'), 'stop-scrolling');
    } else {
      $openMdls[$openMdls.length - 1].focus();
    }
  }

  componentWillUnmount() {
    if(_.hasClass(document.querySelector('body'), 'stop-scrolling') && document.querySelectorAll('.mdl-wrap').length === 0) {
      _.removeClass(document.querySelector('body'), 'stop-scrolling');
    }
  }

  _onClickClose(e) {

    e.preventDefault();
    const { onClose } = this.props;

    _.removeClass(document.querySelector('body'), 'stop-scrolling');

    if (onClose) {
      onClose();
    }
  }

  _onKeyDownHandler(e) {

    const { onClose } = this.props;

    if (e.keyCode === 27) {
      e.stopPropagation();

      onClose();
    }
  }

  render() {

    const { height } = this.state;
    const { isOpen, containerClassName, className, children, noBackdrop, overlayClassName } = this.props;
    const { _onKeyDownHandler, _onClickClose } = this;
    const modalStyle = {};
    const backdropStyle = {};

    if (!isOpen && document.querySelectorAll('.mdl-wrap').length === 0) {
      _.removeClass(document.querySelector('body'), 'stop-scrolling');
    }

    if (height) {
      modalStyle.height = height;
    }

    if (isOpen) {
      _.addClass(document.querySelector('body'), 'stop-scrolling');

      return (
        <div className={ `mdl-wrap ${containerClassName}` } onKeyDown={ _onKeyDownHandler } tabIndex="1">
          <div className={ `mdl-box ${className}` } style={ modalStyle }>
            { children }
          </div>
          {
            !noBackdrop
            && <div className={ `mdl-overlay ${overlayClassName}` } style={ backdropStyle } onClick={ e => _onClickClose(e) }/>
          }
        </div>
      );
    }

    return null;
  }
}
