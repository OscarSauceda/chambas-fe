//
import React from 'react';
import PropTypes from 'prop-types';

// Internal =====================================
const { Component } = React;
const { Select } = Validation.components;

// Dropdown Component ===========================
export default class Dropdown extends Component {
  static propTypes = {
    wrapperClassName: PropTypes.string,
    textClassName: PropTypes.string,
    inputName: PropTypes.string,
    required: PropTypes.bool,
    changeHandler: PropTypes.func,
    val: PropTypes.any
  };

  static defaultProps = {
    wrapperClassName: '',
    textClassName: '',
    inputName: '',
    validationForm: false,
    isRequired: false,
    val: '',
    changeHandler: () => {},
    register: () => {},
    unregister: () => {},
    validateState: () => {}
  };

  constructor(props, context) {

    super(props, context);

    const { children: text, value } =
      Array.isArray(this.props.children)
        ? this.props.children[0].props
        : this.props.children.props;

    this.state = {
      text, value
    };

    this._bind('changeHandler', 'preventIOSFocus', 'renderSelectComponent');
  }

  componentDidMount() {

    const elem = Array
      .from(this.props.children)
      .find(el => el.props.value === this.props.val);

    if (elem) {
      this.setState({
        text: elem.props.children,
        value: elem.props.value
      });
    }
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.val) {

      const elem = Array.from(this.props.children).find(el => el.props.value === nextProps.val);

      if (elem) {
        this.setState({
          text: elem.props.children,
          value: elem.props.value
        });
      }
    }
  }

  preventIOSFocus(e) {

    e.preventDefault();
  }

  changeHandler(e) {

    const { text, value } = Array
      .from(e.target.children)
      .find(el => el.selected);

    this.setState({ text, value }, this.props.changeHandler(e));
  }

  renderSelectComponent(type) {

    const { props, state, changeHandler, preventIOSFocus } = this;
    const { inputName, isRequired, children } = props;
    const { value } = state;
    const opts = [];

    if (Array.isArray(children)) {

      children.forEach((option, index) => {
        opts.push( <option key={ `${inputName}-${index}` } value={ option.props.value }>{ option.props.children }</option> );
      });
    } else {

      opts.push( <option key={ `${inputName}-0` } value={ children.props.value }>{ children.props.children }</option> );
    }


    if (type) {
      return(
        <Select
          name={inputName}
          onChange={ changeHandler }
          onFocus={ preventIOSFocus }
          onBlur={ preventIOSFocus }
          value={ value }
          validations = { ['required'] } >
          { opts }
        </Select>
      );
    }

    return(
      <select
        name={ inputName }
        required={ isRequired }
        onChange={ changeHandler }
        onFocus={ preventIOSFocus }
        onBlur={ preventIOSFocus }
        value={ value } >
        { opts }
      </select>
    );
  }

  render() {

    const { props, state, renderSelectComponent } = this;
    const { wrapperClassName, textClassName, validationForm } = props;
    const { text } = state;

    return (
      <div className={ wrapperClassName + ' ddl-wrap' }>
        <div className={ textClassName + ' ddl-text'}>{ text } <span className="ddl-icon" /> </div>
        { renderSelectComponent(validationForm) }
      </div>
    );
  }
}
