import React, { Component } from 'react';
import Slider from 'react-slick';

export default class ResponsiveSlider extends Component {

  static defaultProps = {
    dots: false,
    centerMode: false,
    infinite: true,
    centerPadding: '60px',
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    lazyLoad: false,
    hasCount: false,
    adaptiveHeight: true,
    variableWidth: false
  };

  constructor(props) {
    super(props);
    this.state = {
      activeIndex: this.props.initialSlide
    };
  }

  updateIndex = (activeIndex) => this.setState({activeIndex});

  render() {

    return (
      <div className={`${this.props.className} slider-container`}>
        <Slider
          afterChange={this.updateIndex}
          {...this.props}>
          {this.props.children}
        </Slider>
        { this.props.hasCount && <div className="slick-count txt-center">{`${this.state.activeIndex + 1} / ${this.props.children.length}`}</div> }
      </div>
    );
  }
}
