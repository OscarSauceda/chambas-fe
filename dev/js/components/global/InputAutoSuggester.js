// deps
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';

// Internal =====================================
const { Component } = React;

// InputAutoSuggester Component ===========================
export default class InputAutoSuggester extends Component {
  static propTypes = {
    suggestionsArray: PropTypes.array.isRequired
  };

  static defaultProps = {
    // Dummy data
    suggestionsArray: [
      {
        name: 'Transporte ',
        year: 1972
      },
      {
        name: 'Cerrajería',
        year: 2000
      },
      {
        name: 'Asistencia Vial',
        year: 1983
      },
      {
        name: 'Carpinteria',
        year: 2007
      },
      {
        name: 'Jardinería',
        year: 2012
      },
      {
        name: 'Fontanería',
        year: 2009
      },
      {
        name: 'Refrigeración',
        year: 1990
      },
      {
        name: 'Masajes',
        year: 1995
      },
      {
        name: 'Buffete Legal',
        year: 1995
      },
      {
        name: 'Hardware/Software',
        year: 1987
      },
      {
        name: 'Diseño Gráfico',
        year: 1995
      },
      {
        name: 'Fletes/Mudanzas',
        year: 1991
      },
      {
        name: 'Sastrería',
        year: 1995
      },
      {
        name: 'Zapatería',
        year: 2003
      }
    ],
    placeholder: 'Fill me',
    inputIcon: 'icon-pin',
    suggestionItemIcon: 'icon-pin'
  };

  constructor(props) {

    super(props);

    this._bind('_getSuggestions', '_getSuggestionValue', '_renderSuggestion', '_onChange', '_onSuggestionsFetchRequested', '_onSuggestionsClearRequested');
  }

  // Autosuggest is a controlled component.
  // This means that you need to provide an input value
  // and an onChange handler that updates this value (see below).
  // Suggestions also need to be provided to the Autosuggest,
  // and they are initially empty because the Autosuggest is closed.
  state = {
    value: '',
    suggestions: []
  };

  // Teach Autosuggest how to calculate suggestions for any given input value.
  _getSuggestions(value) {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;
    const { suggestionsArray } = this.props;
    const regexPattern = new RegExp(inputValue, 'i');

    // return inputLength === 0 ? [] : suggestionsArray.filter(lang =>
    //   lang.name.toLowerCase().slice(0, inputLength) === inputValue
    // );

    return inputLength === 0 ? [] : suggestionsArray.filter(suggestion => regexPattern.test(suggestion.name));
  }

  // When suggestion is clicked, Autosuggest needs to populate the input element
  // based on the clicked suggestion. Teach Autosuggest how to calculate the
  // input value for every given suggestion.
  _getSuggestionValue = (suggestion) => suggestion.name;

  // Use your imagination to render suggestions.
  // _renderSuggestion(suggestion, {query}) {
  _renderSuggestion(suggestion) {

    const { suggestionItemIcon } = this.props;
    // const regexPattern = new RegExp(query, 'i');

    return(
      <div className="suggestion-item">
        <i className={suggestionItemIcon} />
        {suggestion.name}
      </div>
    );
  }

  _onChange = (event, { newValue }) => this.setState({ value: newValue });

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  _onSuggestionsFetchRequested = ({ value }) => this.setState({ suggestions: this._getSuggestions(value) });

  // Autosuggest will call this function every time you need to clear suggestions.
  _onSuggestionsClearRequested = () => this.setState({ suggestions: [] });

  _goToTicket = () => {
    setTimeout( () =>  { window.location.href = `/task/${this.state.value}`; }, 500 );
  }

  render() {

    const { props, state, _onSuggestionsFetchRequested, _onSuggestionsClearRequested, _getSuggestionValue, _renderSuggestion, _onChange, _goToTicket } = this;
    const { value, suggestions } = state;
    const { inputIcon } = props;

    // Autosuggest will pass through all these props to the input element.
    const inputProps = {
      placeholder: 'Dime que buscas:',
      value,
      onChange: _onChange
    };

    // Finally, render it!
    return (
      <div className="auto-suggester-wrapper pr">
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={_onSuggestionsFetchRequested}
          onSuggestionsClearRequested={_onSuggestionsClearRequested}
          onSuggestionSelected={_goToTicket}
          getSuggestionValue={_getSuggestionValue}
          renderSuggestion={_renderSuggestion}
          inputProps={inputProps}
        />
        <i className={inputIcon} />
      </div>
    );
  }
}
