// Deps =========================================
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';

// Internals ====================================
const Component = React.Component;

// Check in & Check out Datepickers
export default class CustomToolTip extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    wrapperClassname: PropTypes.string
  };

  constructor(props) {

    super(props);

    this._bind('_onClickHandler');
  }

  state = {
    isOpen: false
  }

  componentDidMount() {
    window.addEventListener('resize', () => {
      const { isOpen } = this.state;

      if(isOpen) {
        this.setState({ isOpen: false });
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {

    const { state } = this;

    if ( state.isOpen !== nextState.isOpen ) {
      return true;
    }

    return false;
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => {} );
  }

  _onClickHandler(e) {

    const isOpen = (_.hasClass(e.currentTarget, 'open')) ? false : true;

    this.setState({ isOpen });
  }

  render() {

    const { props, state, _onClickHandler } = this;
    const { children, id, className, wrapperClassname } = props;
    const { isOpen } = state;

    return (
      <div className={(wrapperClassname) ? wrapperClassname : '' }>
        <button
          onClick={_onClickHandler}
          data-tip data-for={id}
          className={`custom-information-tooltip txt-center pr ${(isOpen) ? 'open' : ''}`}
          data-offset="{'top': 8}"
          data-event="click"
          data-iscapture
        >
          <i className="icon-information"/>
        </button>
        <ReactTooltip place="top" effect="solid" type="light" id={id} className={className}>
          { children }
        </ReactTooltip>
      </div>
    );
  }
}
