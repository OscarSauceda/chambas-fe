import React from 'react';
import ItemCard from './ItemCard';
import PropTypes from 'prop-types';
import services from  '../../../json/services.json';

export default class TypePanel extends React.Component {

  constructor() {
    super();
  }

  state = {
    itemData: services.data.serviceCategories
  }
  _renderServiceCards = (items) => {
    const itemCards = items.map(item => <ItemCard key={item._id} serviceName={item.name} thumbnail={item.thumbnail} isViewMore={false} /> );

    return itemCards;
  }
  _getCurrentTask = (items) => {
    const currentTask = items.filter(item => item.name === this.props.taskType);

    return currentTask;
  }

  render() {
    const currentTask = this._getCurrentTask(this.state.itemData);
    const cards = this._renderServiceCards(currentTask[0].services);

    return(
      <div className="container txt-center">
        <section className="intro">
          <h5>Contrata fácil</h5>
          <p>Habla con tu proveedor de servicio, después de agendar una cita</p>
        </section>
        <div className="row container container-fluid srv-cards-wrapper">
          {cards}
        </div>
      </div>
    );
  }
}

TypePanel.propTypes = {
  taskType: PropTypes.string
};
