import React from 'react';
import PropTypes from 'prop-types';

export default class ItemCard extends React.Component {

  static propTypes = {
    serviceName: PropTypes.string.isRequired,
    thumbnail: PropTypes.string,
    url: PropTypes.string,
    isViewMore: PropTypes.bool
  };

  static defaultProps = {
    isViewMore: true
  };

  constructor() {
    super();
  }

  render() {
    const { serviceName, thumbnail, url, isViewMore } = this.props;

    return(
      <div className="card xs-12 lg-4">
        <div className="row middle-lg">
          <div className="xs-12 padding-less"><img className="service-pic img-responsive" src={ thumbnail } alt=""/></div>
          <div className="content xs-12">
            <strong className="title">{ serviceName }</strong>
            <p className="description">Apples are more effcient than caffeine in keeping people awake in the morning.</p>
            <div className="row">
              {
                <a href={ isViewMore ? url : '/task/pick-provider' }className="task-details xs-12">
                  {
                    !isViewMore && <i className="icon-bubble" />
                  }
                  {
                    isViewMore ? 'Leer más' : 'Solicitud en Linea'
                  }
                </a>
              }
              {
                !isViewMore && <a href="tel:99999999" className="task-details xs-12">
                  <i className="icon-phone" />
                  Llamanos
                </a>
              }
              {
                !isViewMore &&
                  <a href={`https://api.whatsapp.com/send?phone=50497605850&text=${encodeURIComponent(`Buen dia, quiero más información del servicio de ${serviceName}`)}`}
                    className="task-details xs-12" target="_blank">
                    <i className="icon-whatsapp" />
                  Contactanos por Whatsapp
                  </a>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

