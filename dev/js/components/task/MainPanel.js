import React from 'react';
import ItemCard from './ItemCard';
import services from  '../../../json/services.json';

export default class MainPanel extends React.Component {

  constructor() {
    super();
  }

  state = {
    itemData: services.data.serviceCategories
  }
  _renderServiceCards = (items) => {
    const itemCards = items.map(item => <ItemCard key={item._id} serviceName={item.name} thumbnail={item.thumbnail} url={item.url} /> );

    return itemCards;
  }

  render() {
    const cards = this._renderServiceCards(this.state.itemData);

    return(
      <div className="container txt-center">
        <section className="intro">
          <h5>Elije un servicio</h5>
          <p>Habla con tu proveedor de servicio, después de agendar una cita</p>
        </section>
        <div className="row container container-fluid srv-cards-wrapper">
          {cards}
        </div>
      </div>
    );
  }
}
