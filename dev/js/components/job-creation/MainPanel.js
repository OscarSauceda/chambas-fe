import DropDown from '../global/DropDown';
// deps

export default class MainPanel extends React.Component {

  render() {
    return(
      <div className="job-creation row center-xs">
        <h2 className="job-creation-ttl xs-10">Por favor ingrese la siguiente informacion</h2>
        <label htmlFor="title" className="xs-10 txt-left">Titulo</label>
        <div className="xs-10 lg-4 xl-3 ddl-wrap">
          <input type="text" className="form-control"/>
        </div>
        <label htmlFor="description" className="xs-10 txt-left">Descripcion</label>
        <div className="xs-10 lg-4 xl-3 ddl-wrap">
          <textarea name="" id="description" cols="30" rows="10" className="form-control"/>
        </div>
        <label htmlFor="requirements" className="xs-10 txt-left">Requerimiento</label>
        <DropDown wrapperClassName="xs-10">
          <option value="0" defaultValue>Seleccione una categoria</option>
          <option value="1">Transporte</option>
          <option value="2">Cerrajería</option>
          <option value="3">Asistencia Vial</option>
          <option value="4">Carpinteria</option>
          <option value="5">Jardinería</option>
          <option value="6">Fontanería</option>
          <option value="7">Refrigeración</option>
          <option value="8">Masajes</option>
          <option value="9">Buffete Legal</option>
          <option value="10">Hardware/Software</option>
          <option value="11">Diseño Gráfico</option>
          <option value="12">Fletes/Mudanzas</option>
          <option value="13">Sastrería</option>
          <option value="14">Zapatería</option>
        </DropDown>
        <label htmlFor="budget" className="xs-10 txt-left">Presupuesto</label>
        <div className="xs-10">
          <div className="row start-xs middle-xs">
            <label htmlFor="" className="xs-2">Mon.</label>
            <DropDown wrapperClassName="xs-4">
              <option value="LPS">LPS</option>
              <option value="USD">USD</option>
            </DropDown>
            <label htmlFor="" className="xs-2">Monto</label>
            <div className="xs-4 ddl-wrap">
              <input type="number" className="form-control"/>
            </div>
          </div>
        </div>
        <div className="xs-11">
          <img src="/assets/img/sps-google-maps.gif" alt="" className="img-responsive"/>
        </div>
        <div className="xs-10 lg-4 xl-3">
          <button className="btn-default btn-blue btn-xs gmap-btn">Cambiar Ubicacion</button>
        </div>
        <div className="xs-10 lg-4 xl-3">
          <button className="btn-default btn-blue btn-xs">Crear</button>
        </div>
        <div className="xs-10 lg-4 xl-3">
          <button className="btn-default btn-blue btn-xs">Cancelar</button>
        </div>
      </div>
    );
  }
}

