// Deps =========================================
import AutoSuggester from '../global/InputAutoSuggester';

// Internals ====================================
const Component = React.Component;

// Home-Panel
export default class MainPanel extends Component {

  render() {
    return(
      <div className="row home-panel">
        <div className="xs-12 zero-padding home-main-bg">
          <img className="img-responsive" src="./assets/img/home-bg.jpg" alt=""/>
        </div>
        <div className="xs-12 search-panel">
          <h3>Una manera fácil de encontrar servicios certificados</h3>
          <AutoSuggester />
        </div>
        <div className="xs-12 home-steps txt-center">
          <h2>¿Cómo funciona?</h2>
          <div className="row container margin-auto">
            <div className="xs-12 xl-4 step-item">
              <div className="row middle-lg">
                <div className="xs-12 lg-5">
                  <img className="img-responsive" src="https://lh3.googleusercontent.com/fANnF5Q8uTT_auRLuLNlKjlSa0CwzdnMk0l3zGUuYVfuPbFB2sAwq_G9P70zW8_l2tE=w300" alt=""/>
                </div>
                <p className="xs-12 lg-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi blanditiis mollitia, consequatur suscipit natus possimus.</p>
              </div>
            </div>
            <div className="xs-12 xl-4 step-item">
              <div className="row middle-lg">
                <div className="xs-12 lg-5">
                  <img className="img-responsive" src="https://lh3.googleusercontent.com/fANnF5Q8uTT_auRLuLNlKjlSa0CwzdnMk0l3zGUuYVfuPbFB2sAwq_G9P70zW8_l2tE=w300" alt=""/>
                </div>
                <p className="xs-12 lg-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi blanditiis mollitia, consequatur suscipit natus possimus.</p>
              </div>
            </div>
            <div className="xs-12 xl-4 step-item">
              <div className="row middle-lg">
                <div className="xs-12 lg-5">
                  <img className="img-responsive" src="https://lh3.googleusercontent.com/fANnF5Q8uTT_auRLuLNlKjlSa0CwzdnMk0l3zGUuYVfuPbFB2sAwq_G9P70zW8_l2tE=w300" alt=""/>
                </div>
                <p className="xs-12 lg-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi blanditiis mollitia, consequatur suscipit natus possimus.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
