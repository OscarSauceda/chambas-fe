export default class LoginForm extends React.Component {
  render() {
    return(
      <div className="login-form wrapper row center-xs txt-center">
        <h2 className="dashboard-ttl xs-12">Acceder a traves de:</h2>
        <div className="xs-10 lg-4 xl-3">
          <button className="btn-default btn-blue facebook-btn btn-xs"><i className="icon-facebook"/> Facebook</button>
        </div>
        <div className="xs-10 lg-4 xl-3">
          <button className="btn-default btn-blue google-btn btn-xs"><i className="icon-google"/> Google</button>
        </div>
        <div className="xs-10 lg-4 xl-3">
          <button className="btn-default btn-blue btn-xs">Acceder con usuario</button>
        </div>
        <div className="xs-12">
          <span className="or-copy">O</span>
        </div>
        <div className="xs-10 lg-4 xl-3">
          <button className="btn-default btn-blue btn-xs" onClick={ this.props.renderedComponentHandler('registration') }>Registrase</button>
        </div>
      </div>
    );
  }
}
