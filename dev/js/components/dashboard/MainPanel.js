// deps
import LoginForm from './LoginForm';
import RegistrationForm from './RegistrationForm';

export default class MainPanel extends React.Component {

  constructor() {
    super();
  }

  state = {
    renderComponent: 'login'
  }

  _renderedComponentHandler = (renderComponent) => () => this.setState({ renderComponent });

  render() {
    return(
      <div className="dashboard">
        {
          (this.state.renderComponent === 'login') && <LoginForm renderedComponentHandler={ this._renderedComponentHandler } />
        }
        {
          (this.state.renderComponent === 'registration') && <RegistrationForm />
        }
      </div>
    );
  }
}
