export default class RegistrationForm extends React.Component {
  render() {
    return(
      <div className="registration-form wrapper row center-xs middle-xs">
        <label htmlFor="" className="xs-10">Por favor ingrese la siguiente informacion:</label>
        <label htmlFor="first-name" className="xs-10">Nombre</label>
        <div className="xs-10 lg-6 xl-4 ddl-wrap">
          <input type="text" id="first-name" className="form-control"/>
        </div>
        <label htmlFor="last-name" className="xs-10">Apellido</label>
        <div className="xs-10 lg-6 xl-4 ddl-wrap">
          <input type="text" id="last-name" className="form-control"/>
        </div>
        <label htmlFor="email" className="xs-10">Direccion de correo</label>
        <div className="xs-10 lg-6 xl-4 ddl-wrap">
          <input type="text" id="email" className="form-control"/>
        </div>
        <label htmlFor="user-name" className="xs-10">Usuario</label>
        <div className="xs-10 lg-6 xl-4 ddl-wrap">
          <input type="text" id="user-name" className="form-control"/>
        </div>
        <label htmlFor="password" className="xs-10">Contrasena</label>
        <div className="xs-10 lg-6 xl-4 ddl-wrap">
          <input type="password" id="password" className="form-control"/>
        </div>
        <label htmlFor="confirm-password" className="xs-10">Confirmar contrasena</label>
        <div className="xs-10 lg-6 xl-4 ddl-wrap">
          <input type="password" id="confirm-password" className="form-control"/>
        </div>
        <div className="xs-10">
          <button className="btn-default btn-register btn-blue btn-xs">Registrarse</button>
        </div>
      </div>
    );
  }
}
