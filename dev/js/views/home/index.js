// Deps =========================================
import AutoSuggester from '../../components/global/InputAutoSuggester';
import services from  '../../../json/services.json';

// fetch('https://thawing-spire-65455.herokuapp.com/api/v1.0/service-categories')
//   .then((response) => console.log(response));

console.log(services.data);

const servicesArray = services.data.serviceCategories.map((el) => ({ serviceId: el._id, name: el.name }));

console.log(servicesArray);

// Render All
ReactDOM.render(
  <AutoSuggester
    suggestionsArray={ servicesArray }
  />,
  document.getElementById('root')
);
