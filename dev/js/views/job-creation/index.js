import MainPanel from '../../components/job-creation/MainPanel';

// Render All
ReactDOM.render(
  <MainPanel />,
  document.getElementById('job-creation-panel')
);
