// hacking my way out of this
const $form = document.querySelector('.main-form');

$form.addEventListener('submit', (event) => {
  event.preventDefault();

  const formAction = event.target.dataset.action;

  window.location.href = formAction;
});
