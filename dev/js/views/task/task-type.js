import TypePanel from '../../components/task/TypePanel';

// Render All
ReactDOM.render(
  <TypePanel taskType={_app.page.taskURLParameter} />,
  document.getElementById('task-type-panel')
);
