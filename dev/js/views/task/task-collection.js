import MainPanel from '../../components/task/MainPanel';

// Render All
ReactDOM.render(
  <MainPanel />,
  document.getElementById('task-collection-panel')
);
